FROM python:alpine
RUN apk add --no-cache \
      gcc \
      musl-dev \
      libffi-dev \
      openssl-dev \
      linux-headers; \
    pip install --upgrade pip && \
    pip install openstacksdk boto3;
